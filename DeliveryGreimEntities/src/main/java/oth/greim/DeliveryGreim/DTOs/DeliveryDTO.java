package oth.greim.DeliveryGreim.DTOs;

import oth.greim.DeliveryGreim.Entitys.Address;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeliveryDTO implements Serializable {
    private String usernameDeliveryGreim;
    private String passwordDeliveryGreim;
    private String usernameBogerPay;
    private String passwordBogerPay;
    private String ibanBogerPay;
    private Address source;
    private Address destination;
    private List<DeliveryObjectDTO> deliveryobjectlist = new ArrayList<DeliveryObjectDTO>();
    private boolean expressdelivery;
    private boolean specialdelivery;

    public String getUsernameDeliveryGreim() {
        return usernameDeliveryGreim;
    }

    public void setUsernameDeliveryGreim(String usernameDeliveryGreim) {
        this.usernameDeliveryGreim = usernameDeliveryGreim;
    }

    public String getPasswordDeliveryGreim() {
        return passwordDeliveryGreim;
    }

    public void setPasswordDeliveryGreim(String passwordDeliveryGreim) {
        this.passwordDeliveryGreim = passwordDeliveryGreim;
    }

    public Address getSource() {
        return source;
    }

    public void setSource(Address source) {
        this.source = source;
    }

    public Address getDestination() {
        return destination;
    }

    public void setDestination(Address destination) {
        this.destination = destination;
    }

    public List<DeliveryObjectDTO> getDeliveryobjectlist() {
        return deliveryobjectlist;
    }

    public void setDeliveryobjectlist(List<DeliveryObjectDTO> deliveryobjectlist) {
        this.deliveryobjectlist = deliveryobjectlist;
    }

    public boolean isExpressdelivery() {
        return expressdelivery;
    }

    public void setExpressdelivery(boolean expressdelivery) {
        this.expressdelivery = expressdelivery;
    }

    public boolean isSpecialdelivery() {
        return specialdelivery;
    }

    public void setSpecialdelivery(boolean specialdelivery) {
        this.specialdelivery = specialdelivery;
    }

    public String getUsernameBogerPay() {
        return usernameBogerPay;
    }

    public void setUsernameBogerPay(String usernameBogerPay) {
        this.usernameBogerPay = usernameBogerPay;
    }

    public String getPasswordBogerPay() {
        return passwordBogerPay;
    }

    public void setPasswordBogerPay(String passwordBogerPay) {
        this.passwordBogerPay = passwordBogerPay;
    }

    public String getIbanBogerPay() {
        return ibanBogerPay;
    }

    public void setIbanBogerPay(String ibanBogerPay) {
        this.ibanBogerPay = ibanBogerPay;
    }

    public void addDeliveryObjectDTOToList(DeliveryObjectDTO object) {
        deliveryobjectlist.add(object);
    }
}
