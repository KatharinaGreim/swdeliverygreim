package oth.greim.DeliveryGreim.DTOs;

import javax.validation.constraints.NotNull;
import oth.greim.DeliveryGreim.Entitys.Category;

import java.io.Serializable;

public class DeliveryObjectDTO implements Serializable {
    @NotNull
    private Category category;
    @NotNull
    private String description;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
