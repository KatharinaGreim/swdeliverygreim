package oth.greim.DeliveryGreim.Entitys;

import javax.validation.constraints.NotNull;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DeliveryObject implements Serializable {

    @NotNull
    private Category category;
    @NotNull
    private Double maxweight;
    @NotNull
    private Double price;
    @NotNull
    private Double length;
    @NotNull
    private Double width;
    @NotNull
    private Double height;
    @NotNull
    private String description;

    public DeliveryObject() {
        setCategory(Category.SmallParcel);
        setMaxweight(3.00);
        setPrice(3.00);
        setLength(35.00);
        setWidth(25.00);
        setHeight(15.00);
        setDescription("");
    }

    public DeliveryObject(Category category, String description) {
        setCategory(category);
        setDescription(description);
        switch (category) {
            case SmallParcel:
                setMaxweight(3.00);
                setPrice(3.00);
                setLength(35.00);
                setWidth(25.00);
                setHeight(15.00);
                break;
            case MediumParcel:
                setMaxweight(10.00);
                setPrice(6.00);
                setLength(60.00);
                setWidth(30.00);
                setHeight(15.00);
                break;
            case LargeParcel:
                setMaxweight(30.00);
                setPrice(15.00);
                setLength(120.00);
                setWidth(60.00);
                setHeight(60.00);
                break;
            case StandardDocument:
                setMaxweight(0.020);
                setPrice(0.80);
                setLength(23.00);
                setWidth(11.50);
                setHeight(0.50);
                break;
            case LargeDocument:
                setMaxweight(0.500);
                setPrice(2.00);
                setLength(32.00);
                setWidth(22.50);
                setHeight(2.00);
                break;
            default:
                setMaxweight(0.00);
                setPrice(0.00);
                setLength(0.00);
                setWidth(0.00);
                setHeight(0.00);
                break;
        }
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Double getMaxweight() {
        return maxweight;
    }

    public void setMaxweight(Double maxweight) {
        this.maxweight = maxweight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final DeliveryObject other = (DeliveryObject) o;
        if (!Objects.equals(category, other.category) || !Objects.equals(description, other.description)) {
            return false;
        }
        return true;
    }
}
