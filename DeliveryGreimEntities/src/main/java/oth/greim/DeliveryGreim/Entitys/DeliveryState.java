package oth.greim.DeliveryGreim.Entitys;

public enum DeliveryState {
    CREATED, IN_PROCESS, SENT, DELIVERED
}
