package oth.greim.DeliveryGreim.Entitys;

import oth.greim.DeliveryGreim.DTOs.DeliveryObjectDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Delivery extends SuperclassEntity {
    @NotNull
    @Embedded
    private Address source = new Address();
    @NotNull
    @Embedded
    private Address destination = new Address();
    @NotNull
    @ElementCollection(fetch=FetchType.EAGER)
    private List<DeliveryObject> deliveryobjectlist = new ArrayList<DeliveryObject>();
    @NotNull
    private boolean expressdelivery = false;
    @NotNull
    private boolean specialdelivery = false;
    @NotNull
    private Double totalprice = 0.0;
    @NotNull
    private DeliveryState state = DeliveryState.CREATED;


    public Address getDestination() {
        return destination;
    }

    public void setDestination(Address destination) {
        this.destination = destination;
    }

    public List<DeliveryObject> getDeliveryobjectlist() {
        return deliveryobjectlist;
    }

    public void setDeliveryobjectlist(List<DeliveryObject> deliveryobjectlist) {
        this.deliveryobjectlist = deliveryobjectlist;
        calculateTotalprice();
    }

    public boolean isExpressdelivery() {
        return expressdelivery;
    }

    public void setExpressdelivery(boolean expressdelivery) {
        this.expressdelivery = expressdelivery;
        calculateTotalprice();
    }

    public boolean isSpecialdelivery() {
        return specialdelivery;
    }

    public void setSpecialdelivery(boolean specialdelivery) {
        this.specialdelivery = specialdelivery;
        calculateTotalprice();
    }

    public Address getSource() { return source; }

    public void setSource(Address source) { this.source = source; }

    public Double getTotalprice() { return totalprice; }

    public void setTotalprice(Double totalprice) { this.totalprice = totalprice; }

    public DeliveryState getState() { return state; }

    public void setState(DeliveryState state) { this.state = state; }

    public void calculateTotalprice() {
        totalprice = 0.0;
        for (DeliveryObject object : deliveryobjectlist) {
            totalprice += object.getPrice();
        }
        if (expressdelivery) {
            totalprice += 5.0;
        } else if (specialdelivery) {
            totalprice += 10.0;
        }
    }

    public void addDeliveryObjectToList(DeliveryObject object) {
        deliveryobjectlist.add(object);
        calculateTotalprice();
    }
}
