package oth.greim.DeliveryGreim.Entitys;

import javax.validation.constraints.NotNull;
import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
public class Address implements Serializable {
    @NotNull
    private String name = "";
    @NotNull
    private String streetname = "";
    @NotNull
    private String housenumber = "";
    @NotNull
    private String postcode = "";
    @NotNull
    private String city = "";
    @NotNull
    private String country = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetname() {
        return streetname;
    }

    public void setStreetname(String streetname) {
        this.streetname = streetname;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
