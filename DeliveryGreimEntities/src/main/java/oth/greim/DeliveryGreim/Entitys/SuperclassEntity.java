package oth.greim.DeliveryGreim.Entitys;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@MappedSuperclass
public abstract class SuperclassEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public long getId() {
        return id;
    }

    @Override
    public int hashCode(){
        return Long.hashCode(id);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final SuperclassEntity other = (SuperclassEntity) o;
        if (!Objects.equals(id, other.id)) {
            return false;
        }
        return true;
    }
}
