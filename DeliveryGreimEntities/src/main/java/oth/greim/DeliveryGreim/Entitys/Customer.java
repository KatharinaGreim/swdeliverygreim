package oth.greim.DeliveryGreim.Entitys;

import javax.validation.constraints.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
public class Customer implements UserDetails {
    @Id
    private String email;
    @NotNull
    private String password;
    @NotNull
    private String firstname;
    @NotNull
    private String lastname;
    @NotNull
    @Embedded
    private Address address;
    @NotNull
    @OneToMany(cascade = CascadeType.ALL)
    private List<Delivery> deliverylist = new ArrayList<Delivery>();


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority("CUSTOMER_ROLE"));
    }

    public Address getAddress() { return address; }

    public void setAddress(Address address) { this.address = address; }

    public String getEmail() { return this.email; }

    public void setEmail(String email) { this.email = email; }

    public void setPassword(String password) { this.password = password; }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() { return true; }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public int hashCode(){
        return getEmail().hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final Customer other = (Customer) o;
        if (!Objects.equals(getEmail(), other.getEmail())) {
            return false;
        }
        return true;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<Delivery> getDeliverylist() {
        return deliverylist;
    }

    public void setDeliverylist(List<Delivery> deliverylist) {
        this.deliverylist = deliverylist;
    }

}
