package oth.greim.DeliveryGreim.Entitys;

public enum Category {
    SmallParcel, MediumParcel, LargeParcel, StandardDocument, LargeDocument
}
