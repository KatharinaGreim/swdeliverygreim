package oth.greim.DeliveryGreim.Configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import oth.greim.DeliveryGreim.Entitys.*;
import oth.greim.DeliveryGreim.Services.CustomerServiceIF;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class DataInit implements CommandLineRunner {

    @Autowired
    private CustomerServiceIF customerService;

    @Override
    public void run(String... args) {
        /** Initialize Delivery 1 **/
        Delivery delivery1 = new Delivery();
        DeliveryObject object1 = new DeliveryObject(Category.SmallParcel, "Blumenstrauß");
        DeliveryObject object2 = new DeliveryObject(Category.LargeParcel, "Pralinen");
        DeliveryObject object3 = new DeliveryObject(Category.StandardDocument, "Grußkarte");
        List<DeliveryObject> objectList1 = new ArrayList<DeliveryObject>();
        objectList1.add(object1);
        objectList1.add(object2);
        objectList1.add(object3);
        delivery1.setDeliveryobjectlist(objectList1);
        delivery1.setExpressdelivery(true);

        Address source1 = new Address();
        source1.setName("Greim");
        source1.setStreetname("Brunhuberstraße");
        source1.setHousenumber("4b");
        source1.setPostcode("93053");
        source1.setCity("Regensburg");
        source1.setCountry("Deutschland");
        delivery1.setSource(source1);

        Address destination1 = new Address();
        destination1.setName("Boger");
        destination1.setStreetname("Oberländerstraße");
        destination1.setHousenumber("14");
        destination1.setPostcode("93053");
        destination1.setCity("Regensburg");
        destination1.setCountry("Deutschland");
        delivery1.setDestination(destination1);
        delivery1.setState(DeliveryState.DELIVERED);

        /** Initialize Delivery 2 **/
        Delivery delivery2 = new Delivery();
        DeliveryObject object4 = new DeliveryObject(Category.MediumParcel, "Gemälde");
        List<DeliveryObject> objectList2 = new ArrayList<DeliveryObject>();
        objectList2.add(object4);
        delivery2.setDeliveryobjectlist(objectList2);
        delivery2.setSpecialdelivery(true);

        Address source2 = new Address();
        source2.setName("Müller");
        source2.setStreetname("Müllerstraße");
        source2.setHousenumber("10");
        source2.setPostcode("12345");
        source2.setCity("Müllerstadt");
        source2.setCountry("Deutschland");
        delivery2.setSource(source2);

        Address destination2 = new Address();
        destination2.setName("Meier");
        destination2.setStreetname("Meierstraße");
        destination2.setHousenumber("20");
        destination2.setPostcode("56789");
        destination2.setCity("Meierstadt");
        destination2.setCountry("Deutschland");
        delivery2.setDestination(destination2);
        delivery2.setState(DeliveryState.SENT);

        /** Initialize Customer Katharina Greim **/
        Customer customer = new Customer();
        customer.setEmail("greim@dg.de");
        customer.setPassword("1234");
        customer.setFirstname("Katharina");
        customer.setLastname("Greim");
        Address address = new Address();
        address.setName("Greim");
        address.setStreetname("Brunhuberstraße");
        address.setHousenumber("4b");
        address.setPostcode("93053");
        address.setCity("Regensburg");
        address.setCountry("Deutschland");
        customer.setAddress(address);
        customer.getDeliverylist().add(delivery1);
        customer.getDeliverylist().add(delivery2);
        customerService.createCustomer(customer);

        /** Initialize Customer Florian Klamer **/
        customer = new Customer();
        customer.setEmail("klamer@dg.de");
        customer.setPassword("1234");
        customer.setFirstname("Florian");
        customer.setLastname("Klamer");
        address = new Address();
        address.setName("Klamer");
        address.setStreetname("Brunhuberstraße");
        address.setHousenumber("4b");
        address.setPostcode("93053");
        address.setCity("Regensburg");
        address.setCountry("Deutschland");
        customer.setAddress(address);
        customerService.createCustomer(customer);

        /** Initialize Customer Regina Boger **/
        customer = new Customer();
        customer.setEmail("boger@dg.de");
        customer.setPassword("1234");
        customer.setFirstname("Regina");
        customer.setLastname("Boger");
        address = new Address();
        address.setName("Boger");
        address.setStreetname("Oberländerstraße");
        address.setHousenumber("14");
        address.setPostcode("93053");
        address.setCity("Regensburg");
        address.setCountry("Deutschland");
        customer.setAddress(address);
        customerService.createCustomer(customer);

    }
}
