package oth.greim.DeliveryGreim.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import oth.greim.DeliveryGreim.Entitys.Customer;
import oth.greim.DeliveryGreim.Entitys.Delivery;
import oth.greim.DeliveryGreim.Entitys.DeliveryState;
import oth.greim.DeliveryGreim.Repositorys.CustomerRepository;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("labresources")
public class CustomerService implements CustomerServiceIF, UserDetailsService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public int createCustomer(Customer customer) {
        Optional<Customer> optional = customerRepository.findById(customer.getEmail());
        if (optional.isPresent()) {
            return 0;
        } else {
            customer.setPassword(passwordEncoder.encode(customer.getPassword()));
            customerRepository.save(customer);
            return 1;
        }
    }

    @Override
    public void updateCustomerWithoutDeliveryList(Customer customer) {
        Optional<Customer> optional = customerRepository.findById(customer.getEmail());
        if (optional.isPresent()) {
            Customer current = optional.get();

            current.setFirstname(customer.getFirstname());
            current.setLastname(customer.getLastname());
            current.setAddress(customer.getAddress());

            customerRepository.save(current);
        }
    }

    @Override
    public void updateCustomerDeliveryList(String name, Delivery delivery) {
        Optional<Customer> optional = customerRepository.findById(name);
        if (optional.isPresent()) {
            Customer current = optional.get();
            for (Delivery d : current.getDeliverylist()) {
                if (d.getId() == delivery.getId()) {
                    current.getDeliverylist().remove(d);
                    break;
                }
            }
            current.getDeliverylist().add(delivery);
            customerRepository.save(current);
        }
    }

    @Override
    public Iterable<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    public Customer readById(String email) {
        Optional<Customer> optional = customerRepository.findById(email);
        if (optional.isPresent()) {
            Customer customer = optional.get();
            return customer;
        }
        return null;
    }

    @Override
    public Customer validateCredentials(String email, String password) {

        Optional<Customer> optional = customerRepository.findById(email);

        if (!optional.isPresent()) {
            return null;
        }

        Customer customer = optional.get();

        String customerPassword = customer.getPassword();

        if(!passwordEncoder.matches(password, customerPassword)) {
            return null;
        }

        return customer;

    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Customer> optional = customerRepository.findById(email);
        if (optional.isPresent()) {
            Customer customer = optional.get();
            return customer;
        } else {
            throw new UsernameNotFoundException("Customer with email '" + email + "' does not exist!");
        }
    }
}
