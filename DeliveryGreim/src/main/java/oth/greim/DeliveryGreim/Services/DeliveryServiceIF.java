package oth.greim.DeliveryGreim.Services;

import oth.greim.DeliveryGreim.DTOs.DeliveryDTO;
import oth.greim.DeliveryGreim.DTOs.DeliveryObjectDTO;
import oth.greim.DeliveryGreim.Entitys.Category;
import oth.greim.DeliveryGreim.Entitys.Delivery;
import oth.greim.DeliveryGreim.Entitys.DeliveryObject;
import oth.greim.DeliveryGreim.Entitys.DeliveryState;

import java.util.List;

public interface DeliveryServiceIF {
    void addDeliveryToDeliveryList(String username, Delivery delivery);
    Delivery getCurrentDelivery(String username);
    void addDeliveryObjectToCurrentDelivery(String username, DeliveryObject deliveryObject);
    void deleteDeliveryObjectFromCurrentDelivery(String username, String category, String description);
    void updateCurrentDeliveryWithoutObjectlist(String username, Delivery delivery);
    Long payDelivery(String principalname, String username, String password, String iban);
    void updateCurrentDeliveryObjectlist(String username, Delivery delivery);
    List<DeliveryObject> getDeliveryObjectList(List<DeliveryObjectDTO> dtoList);
    Delivery readById(Long id);
    Long deliver(DeliveryDTO dto);
    boolean additionalService(boolean express, boolean special);
    void updateDeliveryState(String name, Long id, DeliveryState state);
    void simulateTransport(String username, Long deliveryId);
}
