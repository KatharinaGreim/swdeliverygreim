package oth.greim.DeliveryGreim.Services;

import oth.greim.DeliveryGreim.Entitys.Customer;
import oth.greim.DeliveryGreim.Entitys.Delivery;

import java.util.List;

public interface CustomerServiceIF {
    int createCustomer(Customer customer);
    Customer readById(String username);
    Customer validateCredentials(String email, String password);
    void updateCustomerWithoutDeliveryList(Customer customer);
    void updateCustomerDeliveryList(String name, Delivery delivery);
    Iterable<Customer> getAllCustomer();
}
