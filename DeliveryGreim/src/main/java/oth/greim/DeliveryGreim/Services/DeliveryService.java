package oth.greim.DeliveryGreim.Services;

import de.othr.bor.bogerpay.dto.TransactionDTO;
import org.flaxxy.sixrr.dto.NewOrderDTO;
import org.flaxxy.sixrr.entity.ServiceOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import oth.greim.DeliveryGreim.DTOs.DeliveryDTO;
import oth.greim.DeliveryGreim.DTOs.DeliveryObjectDTO;
import oth.greim.DeliveryGreim.Entitys.*;
import oth.greim.DeliveryGreim.Repositorys.CustomerRepository;
import oth.greim.DeliveryGreim.Repositorys.DeliveryRepository;

import java.util.*;

@Service
public class DeliveryService implements DeliveryServiceIF {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private CustomerServiceIF customerService;

    private final RestTemplate restTemplate;

    private final String bogerPayUsernameGreim = "greim@bp.de";
    private final String bogerPayPasswordGreim = "1234";
    private final String bogerPayIBANGreim = "DE50750500000000000003";
    private final String bogerPayIBANKlamer = "DE50750500000000000002";

    private final String IPRegina = "http://im-codd.oth-regensburg.de:8856";
    private final String IPFelix = "http://im-codd.oth-regensburg.de:8857";

    private final String sixrrUsername = "greim";
    private final String sixrrPassword = "greim";

    public DeliveryService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void addDeliveryToDeliveryList(String username, Delivery delivery) {
        customerService.updateCustomerDeliveryList(username, delivery);
    }

    @Override
    public void updateCurrentDeliveryWithoutObjectlist(String username, Delivery delivery) {
        Delivery current = getCurrentDelivery(username);
        if (current != null) {
            current.setDestination(delivery.getDestination());
            current.setSource(delivery.getSource());
            current.setExpressdelivery(delivery.isExpressdelivery());
            current.setSpecialdelivery(delivery.isSpecialdelivery());
            current.setState(delivery.getState());

            customerService.updateCustomerDeliveryList(username, current);
        }
    }

    @Override
    public void updateCurrentDeliveryObjectlist(String username, Delivery delivery) {
        Delivery current = getCurrentDelivery(username);
        if (current != null) {
            current.setDeliveryobjectlist(delivery.getDeliveryobjectlist());
            customerService.updateCustomerDeliveryList(username, current);
        }
    }

    @Override
    public Delivery getCurrentDelivery(String username) {
        Optional optional = customerRepository.findById(username);
        if (optional.isPresent()) {
            Customer customer = (Customer) optional.get();
            if (!customer.getDeliverylist().isEmpty()) {
                for (Delivery d : customer.getDeliverylist()) {
                    if (d.getState() == DeliveryState.CREATED) {
                        return d;
                    }
                }
            }
            return null;
        }
        return null;
    }

    @Override
    public void addDeliveryObjectToCurrentDelivery(String username, DeliveryObject deliveryObject) {
        Delivery current = getCurrentDelivery(username);
        if (current != null) {
            current.getDeliveryobjectlist().add(deliveryObject);
            updateCurrentDeliveryObjectlist(username, current);
        }
    }

    @Override
    public void deleteDeliveryObjectFromCurrentDelivery(String username, String category, String description) {
        Delivery current = getCurrentDelivery(username);
        if (current != null) {
            for (DeliveryObject object : current.getDeliveryobjectlist()) {
                if (object.getCategory().name().equals(category) && object.getDescription().equals(description)) {
                    current.getDeliveryobjectlist().remove(object);
                    break;
                }
            }
            updateCurrentDeliveryObjectlist(username, current);
        }
    }

    @Override
    public List<DeliveryObject> getDeliveryObjectList(List<DeliveryObjectDTO> dtoList) {
        List<DeliveryObject> list = new ArrayList<DeliveryObject>();

        for (DeliveryObjectDTO dto : dtoList) {
            DeliveryObject newObject = new DeliveryObject(dto.getCategory(), dto.getDescription());
            list.add(newObject);
        }

        return list;
    }

    @Override
    public Delivery readById(Long id) {
        Optional<Delivery> optional = deliveryRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

    @Override
    public Long deliver(DeliveryDTO dto) {
        if (dto != null) {
            try {
                Customer customer = customerService.validateCredentials(dto.getUsernameDeliveryGreim(), dto.getPasswordDeliveryGreim());
                Delivery delivery = new Delivery();
                delivery.setSource(dto.getSource());
                delivery.setDestination(dto.getDestination());
                if (dto.isExpressdelivery()) {
                    delivery.setExpressdelivery(true);
                } else if (dto.isSpecialdelivery()){
                    delivery.setSpecialdelivery(true);
                }
                delivery.setDeliveryobjectlist(getDeliveryObjectList(dto.getDeliveryobjectlist()));
                delivery.calculateTotalprice();
                addDeliveryToDeliveryList(dto.getUsernameDeliveryGreim(), delivery);
                Long deliveryId = payDelivery(dto.getUsernameDeliveryGreim(), dto.getUsernameBogerPay(), dto.getPasswordBogerPay(), dto.getIbanBogerPay());

                if (deliveryId != null) {
                    if (dto.isExpressdelivery() || dto.isSpecialdelivery()) {
                        if (!additionalService(dto.isExpressdelivery(), dto.isSpecialdelivery())) {
                            return null;
                        }
                    }
                } else {
                    return null;
                }

                simulateTransport(dto.getUsernameDeliveryGreim(), deliveryId);

                return deliveryId;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    @Override
    public void simulateTransport(String username, Long deliveryId) {
        updateDeliveryState(username, deliveryId, DeliveryState.IN_PROCESS);
    }

    @Override
    public Long payDelivery(String principalname, String username, String password, String iban) {
        Delivery current = getCurrentDelivery(principalname);
        if (current != null) {
            TransactionDTO dto = new TransactionDTO();
            dto.setAmount(current.getTotalprice());
            dto.setRecipientIBAN(bogerPayIBANGreim);
            dto.setRecipientName("DeliveryGreim");
            dto.setConsignorIBAN(iban);
            dto.setReference("Delivery " + current.getId());
            try {
                ResponseEntity<TransactionDTO> success = restTemplate.postForEntity(IPRegina + "/api/transaction?email=" + username +"&password=" + password, dto, TransactionDTO.class);
                success.getStatusCode().is2xxSuccessful();
                return current.getId();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public boolean additionalService(boolean express, boolean special) {
        ServiceOffer[] offers = null;
        String description = "";

        try {
            if (express) {
                offers = restTemplate.getForObject(IPFelix + "/api/searchOffers?query={q}", ServiceOffer[].class, "#express");
                description = "express delivery";
            } else if (special) {
                offers = restTemplate.getForObject(IPFelix + "/api/searchOffers?query={q}", ServiceOffer[].class, "#special");
                description = "special delivery";
            }

            if (offers != null) {
                NewOrderDTO dto = new NewOrderDTO(offers[0].getId());
                dto.setAmount(20);
                dto.setRequestDescription(description);

                ResponseEntity<Boolean> request = restTemplate.postForEntity(IPFelix + "/api/orders?username={u}&password={p}", dto, boolean.class, sixrrUsername, sixrrPassword);

                return request.getStatusCode().is2xxSuccessful();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void updateDeliveryState(String name, Long id, DeliveryState state) {
        Optional<Customer> optional = customerRepository.findById(name);
        if (optional.isPresent()) {
            Customer user = optional.get();
            for(Delivery d : user.getDeliverylist()) {
                if (d.getId() == id) {
                    d.setState(state);
                    deliveryRepository.save(d);
                    break;
                }
            }
            customerRepository.save(user);
        }
    }
}
