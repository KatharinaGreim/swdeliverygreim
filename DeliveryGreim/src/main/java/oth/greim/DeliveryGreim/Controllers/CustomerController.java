package oth.greim.DeliveryGreim.Controllers;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import oth.greim.DeliveryGreim.Entitys.Address;
import oth.greim.DeliveryGreim.Entitys.Customer;
import oth.greim.DeliveryGreim.Services.CustomerServiceIF;
import oth.greim.DeliveryGreim.Services.DeliveryServiceIF;

import java.security.Principal;

@Controller
public class CustomerController {
    @Autowired
    private CustomerServiceIF customerService;

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public String register(@ModelAttribute("email") String email,
                           @ModelAttribute("password") String password,
                           @ModelAttribute("firstname") String firstname,
                           @ModelAttribute("lastname") String lastname,
                           @ModelAttribute("streetname") String streetname,
                           @ModelAttribute("housenumber") String housenumber,
                           @ModelAttribute("postcode") String postcode,
                           @ModelAttribute("city") String city,
                           @ModelAttribute("country") String country,
                           @NotNull Model model){

        Customer customer = new Customer();
        customer.setEmail(email);
        customer.setPassword(password);
        customer.setFirstname(firstname);
        customer.setLastname(lastname);

        Address address = new Address();
        address.setName(lastname);
        address.setStreetname(streetname);
        address.setHousenumber(housenumber);
        address.setPostcode(postcode);
        address.setCity(city);
        address.setCountry(country);
        customer.setAddress(address);
        model.addAttribute("customer", customer);
        int state = customerService.createCustomer(customer);
        if (state == 0) {
            return "error";
        } else {
            return"login";
        }
    }

    @RequestMapping(value="/login", method = RequestMethod.POST)
    public String login(){
        return "login";
    }

    @RequestMapping("/customer/home")
    public String customerHome() {
        return "home";
    }

    @RequestMapping(value = "/overview")
    public String overview(Principal principal,
                           @NotNull Model model) {
        Customer user = customerService.readById(principal.getName());
        model.addAttribute("user", user);
        return "overview";
    }

    @RequestMapping("/updateUser")
    public String updateUser(Principal principal,
                             @ModelAttribute("user") Customer user,
                             @NotNull Model model) {
        if (user != null) {
            customerService.updateCustomerWithoutDeliveryList(user);
        }
        Customer updatedUser = customerService.readById(principal.getName());
        model.addAttribute("user", updatedUser);
        return "overview";
    }
}
