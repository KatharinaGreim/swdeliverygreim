package oth.greim.DeliveryGreim.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import oth.greim.DeliveryGreim.DTOs.DeliveryDTO;
import oth.greim.DeliveryGreim.Entitys.Customer;
import oth.greim.DeliveryGreim.Entitys.Delivery;
import oth.greim.DeliveryGreim.Services.CustomerService;
import oth.greim.DeliveryGreim.Services.CustomerServiceIF;
import oth.greim.DeliveryGreim.Services.DeliveryServiceIF;

import java.util.List;

@RestController
public class DeliveryRestController {
    @Autowired
    private DeliveryServiceIF deliveryService;

    @PostMapping("/api/deliver")
    @ResponseBody
    public Long deliver(
            @RequestBody() DeliveryDTO dto
    ) {
        return deliveryService.deliver(dto);
    }
}
