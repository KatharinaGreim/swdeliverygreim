package oth.greim.DeliveryGreim.Controllers;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import oth.greim.DeliveryGreim.Entitys.*;
import oth.greim.DeliveryGreim.Services.DeliveryServiceIF;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class DeliveryController {
    @Autowired
    private DeliveryServiceIF deliveryService;

    @RequestMapping("/delivery/tracking")
    public String tracking() {
        return "tracking";
    }

    @RequestMapping("/trackId")
    public String trackId(@ModelAttribute("id") String id,
                          @NotNull Model model)
    {
        if (id != null && !id.equals("")) {
            Long longId = Long.parseLong(id);
            Delivery delivery = deliveryService.readById(longId);
            if (delivery != null) {
                if (delivery.getState() == DeliveryState.IN_PROCESS) {
                    model.addAttribute("process", true);
                    model.addAttribute("id", longId);
                    model.addAttribute("notfound", false);
                } else if (delivery.getState() == DeliveryState.SENT) {
                    model.addAttribute("sent", true);
                    model.addAttribute("id", longId);
                    model.addAttribute("notfound", false);
                } else if (delivery.getState() == DeliveryState.DELIVERED) {
                    model.addAttribute("delivered", true);
                    model.addAttribute("id", longId);
                    model.addAttribute("notfound", false);
                } else {
                    model.addAttribute("notfound", true);
                }
            } else {
                model.addAttribute("notfound", true);
            }
            model.addAttribute("id", longId);
        } else {
            model.addAttribute("notfound", true);
        }
        return "tracking";
    }

    @RequestMapping("/delivery/shipping")
    public String shipping(Principal principal,
                           @NotNull Model model) {
        List<Category> categoryList = Arrays.asList(Category.values());
        model.addAttribute("categoryList", categoryList);

        Delivery current = deliveryService.getCurrentDelivery(principal.getName());
        if (current == null) {
            Delivery delivery = new Delivery();
            model.addAttribute("delivery", delivery);
            //deliveryService.addDeliveryToDeliveryList(principal.getName(), delivery);
        } else {
            model.addAttribute("delivery", current);
        }
        //model.addAttribute("delivery", deliveryService.getCurrentDelivery(principal.getName()));
        return "shipping";
    }

    @RequestMapping("/delivery/parcels")
    public  String parcels(@NotNull Model model) {
        List<DeliveryObject> parcels = new ArrayList<DeliveryObject>();

        parcels.add(new DeliveryObject(Category.SmallParcel, ""));
        parcels.add(new DeliveryObject(Category.MediumParcel, ""));
        parcels.add(new DeliveryObject(Category.LargeParcel, ""));

        List<DeliveryObject> documents = new ArrayList<DeliveryObject>();
        documents.add(new DeliveryObject(Category.StandardDocument, ""));
        documents.add(new DeliveryObject(Category.LargeDocument, ""));

        model.addAttribute("parcellist", parcels);
        model.addAttribute("documentlist", documents);

        return "parcels";
    }

    @RequestMapping("/delivery/services")
    public String services() {
        return "services";
    }

    @RequestMapping(value = "/finishDelivery", method = RequestMethod.POST)
    public String finishDelivery(Principal principal,
                                 @ModelAttribute("delivery") Delivery delivery,
                                 @ModelAttribute("detail") String detail,
                                 @NotNull Model model)
    {
        if (detail.equals("express")) {
            delivery.setExpressdelivery(true);
        } else if (detail.equals("special")) {
            delivery.setSpecialdelivery(true);
        }

        Delivery current = deliveryService.getCurrentDelivery(principal.getName());
        if (current == null) {
            deliveryService.addDeliveryToDeliveryList(principal.getName(), delivery);
        } else {
            deliveryService.updateCurrentDeliveryWithoutObjectlist(principal.getName(), delivery);
        }

        model.addAttribute("created", true);
        model.addAttribute("totalprice", deliveryService.getCurrentDelivery(principal.getName()).getTotalprice());
        return "shipping";
    }

    @RequestMapping(value = "/payDelivery", method = RequestMethod.POST)
    public String payDelivery(Principal principal,
                              @ModelAttribute("bogerpayUsername") String bpUsername,
                              @ModelAttribute("bogerpayPassword") String bpPassword,
                              @ModelAttribute("bogerpayIban") String bpIban,
                              @NotNull Model model)
    {
        Delivery delivery = deliveryService.getCurrentDelivery(principal.getName());
        boolean express = delivery.isExpressdelivery();
        boolean special = delivery.isSpecialdelivery();
        Long deliveryId = deliveryService.payDelivery(principal.getName(), bpUsername, bpPassword, bpIban);
        if (deliveryId != null) {
            if (express || special) {
                if (deliveryService.additionalService(express, special)) {
                    model.addAttribute("paid", true);
                    deliveryService.simulateTransport(principal.getName(), deliveryId);
                } else {
                    model.addAttribute("serviceError", true);
                    model.addAttribute("paid", false);
                }
            } else {
                model.addAttribute("paid", true);
                deliveryService.simulateTransport(principal.getName(), deliveryId);
            }
        } else {
            model.addAttribute("payError", true);
        }
        model.addAttribute("created", false);
        model.addAttribute("deliveryId", deliveryId);
        return "shipping";
    }


    @GetMapping(value = "/deleteDeliveryObject/{deleteCategory}/{deleteDescription}")
    public String deleteDeliveryObject(Principal principal,
                                       @PathVariable("deleteDescription") String description,
                                       @PathVariable("deleteCategory") String category,
                                       @NotNull Model model) {
        deliveryService.deleteDeliveryObjectFromCurrentDelivery(principal.getName(), category, description);

        model.addAttribute("delivery", deliveryService.getCurrentDelivery(principal.getName()));
        List<Category> categoryList = Arrays.asList(Category.values());
        model.addAttribute("categoryList", categoryList);
        return "shipping";
    }

    @RequestMapping(value = "/addDeliveryObject")
    public String addDeliveryObject(Principal principal,
                                    @ModelAttribute("delivery") Delivery delivery,
                                    @ModelAttribute("addCategory") Category category,
                                    @ModelAttribute("addDescription") String description,
                                    @NotNull Model model
    ) {
        Delivery current = deliveryService.getCurrentDelivery(principal.getName());
        if (current == null) {
            deliveryService.addDeliveryToDeliveryList(principal.getName(), delivery);
        } else {
            deliveryService.updateCurrentDeliveryWithoutObjectlist(principal.getName(), delivery);
        }
        if (category != null && description != null) {
            DeliveryObject object = new DeliveryObject(category, description);
            deliveryService.addDeliveryObjectToCurrentDelivery(principal.getName(), object);
        }
        model.addAttribute("delivery", deliveryService.getCurrentDelivery(principal.getName()));
        List<Category> categoryList = Arrays.asList(Category.values());
        model.addAttribute("categoryList", categoryList);
        return "shipping";
    }
}
