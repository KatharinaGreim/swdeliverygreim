package oth.greim.DeliveryGreim.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import oth.greim.DeliveryGreim.Entitys.Address;
import oth.greim.DeliveryGreim.Entitys.Customer;
import oth.greim.DeliveryGreim.Services.CustomerServiceIF;

@Controller
public class MainController {
    @Autowired
    private CustomerServiceIF customerService;

    @RequestMapping("/")
    public String start() {
        return "start"; }

    @RequestMapping(value = "/loginSite")
    public  String loginSite() { return "login"; }

    @RequestMapping("/logout")
    public  String logout() { return "start"; }

    @RequestMapping("/registerSite")
    public  String registerSite() { return "registration"; }
}
