package oth.greim.DeliveryGreim.Repositorys;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import oth.greim.DeliveryGreim.Entitys.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String> {
}
