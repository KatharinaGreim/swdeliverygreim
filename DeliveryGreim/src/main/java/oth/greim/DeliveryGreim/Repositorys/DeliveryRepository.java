package oth.greim.DeliveryGreim.Repositorys;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import oth.greim.DeliveryGreim.Entitys.Delivery;

@Repository
public interface DeliveryRepository extends CrudRepository<Delivery, Long> {
}
