package oth.greim.DeliveryGreim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliveryGreimApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeliveryGreimApplication.class, args);
	}

}
